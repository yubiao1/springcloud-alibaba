package cn.itsource.feign.clients;

import cn.itsource.domain.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@FeignClient ：标记接口为Feign的客户端接口，用来调用用户服务 : user-server目标服务的名字
@FeignClient("user-server")
public interface UserClient {

    //调用目标服务的controller的方法
    @GetMapping("/user/{id}")
    User getById(@PathVariable Long id);
}
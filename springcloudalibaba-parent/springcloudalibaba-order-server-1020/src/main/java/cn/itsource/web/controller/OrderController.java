package cn.itsource.web.controller;

import cn.itsource.domain.User;
import cn.itsource.feign.clients.UserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

    @Autowired
    private UserClient userClient ;

    //浏览器
    @RequestMapping("/order/{id}")
    public User getById(@PathVariable Long id){
        //调用user
        return userClient.getById(id);
    }
}
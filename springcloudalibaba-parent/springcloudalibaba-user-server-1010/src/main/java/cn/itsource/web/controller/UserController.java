package cn.itsource.web.controller;

import cn.itsource.domain.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

//用户服务
@RestController
public class UserController {

    @GetMapping("/user/{id}")
    public User getById(@PathVariable Long id){
        return new User(id,"zs:"+id, "我是zs");
    }
}
